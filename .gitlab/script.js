const fs = require("fs");

const FILE_NAME = "dynamic-template.yml";

function generateBeforeScript(folder = folderName) {
  return `
  before_script:
    - cd ${folder}
    - npm ci
  `;
}

function main() {
  console.log("CI_COMMIT_BRANCH", process.env.CI_COMMIT_BRANCH);

  const [, folderName, env = ""] = process.env.CI_COMMIT_BRANCH.split("/");

  console.log("FOLDER_NAME", folderName);
  console.log("ENV", env);

  const template = `
include:
  - local: '.gitlab/refs.yml'

image: node:lts-alpine

cache: &global_cache
  key: $CI_COMMIT_REF_SLUG
  paths:
    - node_modules/
    - /*/node_modules/
  policy: pull

stages:
  - Test

test:
  cache:
    <<: *global_cache
  stage: Test
  ${generateBeforeScript(folderName)}
  script:
    - npm test
`;

  fs.writeFileSync(FILE_NAME, template, { encoding: "utf-8" });
}

main();
